import Control.Concurrent

import XMonad

import XMonad.Util.EZConfig
import XMonad.Util.Ungrab

import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.BinarySpacePartition
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce


myLayout = spacingConfig $ myTall ||| myBSP ||| myThreeCol |||myFull
    where spacingConfig = spacingRaw False (Border 5 0 5 0) True (Border 0 5 0 5) True 
          myTall = Tall 1 (3/100) (1/2)
          myBSP = emptyBSP
          myFull = Full
          myThreeCol = ThreeCol 1 (3/100) (1/2)

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "feh --bg-scale ~/vapor-mountain-4k.jpg"


myConfig =  def
	{ modMask = mod4Mask,
	  terminal = "kitty",
	  layoutHook = myLayout,
      startupHook = myStartupHook,
      borderWidth = 2,
      focusedBorderColor = "#cd8b00"
    }

main :: IO ()
main = do
    -- Launching xmobar on monitor
    xmproc <- spawnPipe "xmobar"
    mons <- spawnPipe "mons --primary HDMI-1 && mons -o"
    -- the xmonad, ya know...what the WM is named after!
    xmonad . xmobarProp $ myConfig


